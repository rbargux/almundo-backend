NODE_ENV ={
    "development": {
        "SERVERURL": "http://localhost/",
            "PORT": 8585
    },
    "test": {
        "SERVERURL": "http://almundoTest/",
            "PORT": 8282
    },
    "prepro": {
        "SERVERURL": "http://almundoPrepro/",
        "PORT": 8181
    },
    "production": {
        "SERVERURL": "http://almundoProdu/",
        "PORT": 8686
    }
}