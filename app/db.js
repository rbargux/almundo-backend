// @flow
(function () {
    'use strict';
    const mongoose = require('mongoose');
    module.exports.connect = () => {
        // Create the database connection
        mongoose.connect("mongodb://almundo:almundo@ds255715.mlab.com:55715/almundo");
        const connection = mongoose.connection;
        let connected = () => { console.log('mongoLab connection success'); };
        let disconnected = () => { console.log('mongoLab disconnect'); }
        let error = (err) => { console.log('Mongoose connection failed: detail\n', err); };
        let nodeProcessEnd = () => {
            mongoose.connection.close(() => {
                console.log('Mongoose default connection disconnected through app termination');
                process.exit(0);
            });
        }
        connection.on('connected', connected);
        connection.on('disconnected', disconnected);
        connection.on('error', error);
        process.on('SIGINT', nodeProcessEnd);
    }
})();