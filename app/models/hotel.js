const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var hotelSchemaJSON = {
    name: String
    , stars: Number
    , url: String
    , price: String
    , latitud: String
    , longitud: String
    , titulomapa: String
    , descripcionmapa: String
}

var hotel_schema = new Schema(hotelSchemaJSON);

var Hotel = mongoose.model("Hotel", hotel_schema);

module.exports.Hotel = Hotel;