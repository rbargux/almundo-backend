(function () {
    'use strict';
    const express = require("express");
    const bodyParser = require("body-parser");
    const app = express();
    const cors = require("cors");
    const Hotel = require("./app/models/hotel").Hotel;
    const { connect } = require('./app/db');
    
    connect();

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cors());

    app.get("/api/hotels", function (req, res) {
        Hotel.find(function (err, respuesta) {
            res.send(respuesta);
        });
    });
    app.get("/api/hotels/:id", function (req, res) {
        Hotel.find({ "_id": req.params.id }, function (err, respuesta) {
            if (!err) {
                res.send(respuesta);
            } else {
                return console.log(err);
            }
        });
    });
    app.post("/api/hotels", function (req, res) {
        var hotel = new Hotel({
            name: req.body.name || ''
            , stars: req.body.stars || 0
            , url: req.body.url || ''
            , price: req.body.price || '0'
            , latitud: req.body.latitud || '0'
            , longitud: req.body.longitud || '0'
            , titulomapa: req.body.titulomapa || ''
            , descripcionmapa: req.body.descripcionmapa || ''
        });
        hotel.save(function () {
            res.send("Guardamos el hotel");
        })
    });

    app.listen(8585);

})();