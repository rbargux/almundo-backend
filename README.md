# Examen Ingreso Almundo (Back-End)

Express es una infraestructura de aplicaciones web Node.js mínima y flexible que proporciona un conjunto sólido de características para las aplicaciones web y móviles.


## Descargar.
Clone the repo via ssh: git clone git@gitlab.com:rbargux/almundo-backend.git


### Quick Start

1. Ejecuta el comando `npm install`.
2. Ejecuta el comando `npm start`.
3. Empieza a realizar peticiones a [http://localhost:8585][local] y voilà.

[nodejs]: https://nodejs.org
[express]: http://expressjs.com/es/
[local]: http://localhost:8585

### Ejemplo

Petición GET http://localhost:8585/api/hotels, la cual devuelve todos los hoteles disponibles.

